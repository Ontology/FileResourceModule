﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace FileResourceModule
{
    public partial class frmFileResourceModule : Form
    {
        private clsLocalConfig objLocalConfig;

        private UserControl_OItemList objUserControl_FileResources;

        private UserControl_FileResource_File objUserControl_FileResource_File;
        private UserControl_FileResource_Path objUserControl_FileResource_Path;


        public frmFileResourceModule()
        {
            InitializeComponent();

            
            Initialize();
        }

        public frmFileResourceModule(clsLocalConfig LocalConfig)
        {
            InitializeComponent();

            objLocalConfig = LocalConfig;
            Initialize();
        }

        public frmFileResourceModule(Globals Globals)
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }
            
            objUserControl_FileResources = new UserControl_OItemList(objLocalConfig.Globals);
            objUserControl_FileResources.Dock = DockStyle.Fill;
            SplitContainer1.Panel1.Controls.Add(objUserControl_FileResources);
            objUserControl_FileResources.Selection_Changed +=objUserControl_FileResources_Selection_Changed;

            objUserControl_FileResource_File = new UserControl_FileResource_File(objLocalConfig);
            objUserControl_FileResource_File.Dock = DockStyle.Fill;
            TabPage_File.Controls.Add(objUserControl_FileResource_File);

            objUserControl_FileResource_Path = new UserControl_FileResource_Path(objLocalConfig);
            objUserControl_FileResource_Path.Dock = DockStyle.Fill;
            TabPage_Path.Controls.Add(objUserControl_FileResource_Path);

            var objOItem_FileResource = new clsOntologyItem
                {
                    GUID_Parent = objLocalConfig.OItem_class_fileresource.GUID,
                    Type = objLocalConfig.Globals.Type_Object
                };

            objUserControl_FileResources.initialize(objOItem_FileResource);
        }

    void objUserControl_FileResources_Selection_Changed()
    {
 	    ConfigureTabPages();
    }

        private void ConfigureTabPages()
        {
            clsOntologyItem objOItem_FileResource = null;

            if (objUserControl_FileResources.DataGridViewRowCollection_Selected.Count == 1)
            {
                var objDGVR_Selected =
                    (DataGridViewRow) objUserControl_FileResources.DataGridViewRowCollection_Selected[0];
                objOItem_FileResource = (clsOntologyItem) objDGVR_Selected.DataBoundItem;

            }


            if (TabControl1.SelectedTab.Name == TabPage_File.Name)
            {
                objUserControl_FileResource_File.Initialize_File(objOItem_FileResource);
            }
            else if (TabControl1.SelectedTab.Name == TabPage_Path.Name)
            {
                objUserControl_FileResource_Path.Initialize_Path(objOItem_FileResource);
            }
            else if (TabControl1.SelectedTab.Name == TabPage_WebConnection.Name)
            {
                
            }
        }

        private void ToolStripButton_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigureTabPages();
        }
    }
}
